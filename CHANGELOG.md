# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## 2018-07-03
### Changed
- 1対1チャットの追加
  - 色々動作が不安定
  - チャットルームにいるときにサイドメニューへユーザー一覧を表示するように変更
  - TODO: repl駆動でやろうとすると色々おかしくなる
- ユーザーのアバターをセッティング出来るように追加
  - チャットアイコン表示の際にiconがセットされていればそちらを使うように変更
- パスワードリセット機能を追加
  - ログイン画面にパスワード発行用画面へのリンクを追加
  - 環境変数にメール設定がある場合はトークン付きURLをメールで通知するように
- セッションストアをメモリとRedisで選択可能に変更
- 各APIキーを環境変数から取得するように変更
- DBコネクションの設定をComponent化し使い回すように変更
- マイグレーションをコンポーネント起動時に実行するように変更
- ルーティングをCompojureからbidiに変更
  - URLのパスをbidiの設定から取得すように変更
  - earth-clj.routerにてルーティングを設定後、earth-clj.router.dispatcherにて各処理にディスパッチするように
- SQLの組み立て時にHoneySQLを利用するように変更
  - 書きっぷりが統一されていない


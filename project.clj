(defproject earth-clj "0.1.0-SNAPSHOT"
  :min-lein-version "2.11.3"
  :uberjar-name "earth-clj.jar"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.11.3"]
                 [org.clojure/data.json "2.5.0"]
                 [commons-codec "1.17.0"]
                 [cljsjs/cropper "0.8.1-0"]
                 [ring "1.12.2"]
                 [crypto-random "1.2.1"]
                 [com.taoensso/carmine "3.4.1"]
                 [com.draines/postal "2.0.5"]
                 [cljs-ajax "0.8.4"]
                 [com.stuartsierra/component "1.1.0"]
                 [honeysql "1.0.461"]
                 [bidi "2.1.6"]
                 [cljsjs/d3 "7.6.1-0"]
                 [environ "1.2.0"]
                 [hiccup "1.0.5"]
                 [org.clojure/java.jdbc "0.7.12"]
                 [bouncer "1.0.1"]
                 [ring/ring-defaults "0.5.0"]
                 [gravatar "1.1.1"]
                 [metosin/ring-http-response "0.9.4"]
                 [slingshot "0.12.2"]
                 [cljsjs/leaflet "1.7.1-0"]
                 [buddy "2.0.0"]
                 [migratus "1.5.6"]
                 [cljsjs/moment "2.29.4-0"]
                 [potemkin "0.4.7"]
                 [http-kit "2.8.0"]
                 [com.taoensso/sente "1.19.2"]
                 [cljsjs/chartjs "2.9.4-0"]
                 [reagent "1.2.0"]
                 [org.clojure/clojurescript "1.11.132"]
                 [org.postgresql/postgresql "42.7.3"]]
  :plugins [[lein-environ "1.2.0"]
            [migratus-lein "0.7.3"]
            [lein-update-dependency "0.1.2"]
            [lein-cljsbuild "1.1.8"]]
  :migratus {:store :database
             :migration-dir "migrations"
             :db {:classname "com.postgresql.Driver"
                  :subprotocol "postgres"
                  :subname "//localhost:5432/earth_clj_dev"
                  :user "postgres"
                  :password "postgres"}}
  :cljsbuild
  {:builds [{:source-paths ["src/earth_clj/cljs"]
             :compiler {:main "earth-cljs.core"
                        :asset-path "/js"
                        :output-to "resources/public/js/main.js"
                        :output-dir "resources/public/js"
                        :source-map true
                        :optimizations :none
                        :pretty-print true}}]}
  :profiles
  {:dev {:dependencies [[prone "2021-04-23"]]
         :source-paths ["dev"]
         ;; 基本的に文字列しか許されない
         :env {:dev "true"
               :owm-key "9f70e15ce517f3eb8a5c50dabd8eaf57"
               :wolfram-id "WYUHQJ-7AYVG6EV4Q"
               :mail-from ""
               :mail-host "smtp.gmail.com"
               :mail-user ""
               :mail-pass ""
               :mail-ssl "true"
               :mail-port "465"
               :session-store "redis"
               :session-timeout "604800" ;; 一週間
               :redis "redis://localhost:6379"
               :db "{:dbtype \"postgresql\"
                     :classname \"org.postgresql.Driver\"
                     :dbname \"earth_clj_dev\"
                     :host \"localhost\"
                     :port 5432
                     :user \"postgres\"
                     :password \"postgres\"}"}}
   :uberjar {:aot :all
             :prep-tasks ["compile" ["cljsbuild" "once"]]
             :main earth-clj.main}})

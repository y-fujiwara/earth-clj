# earth-clj

***DEMO:***

[Demo](https://earth-clj.herokuapp.com/)

## Description

* `http://localhost:4000`
    * 5 day / 3 hour forecast
    * temperature graph
    * current weather
    * weather map
    * search city
        * if you logged in, you can bookmark the search word 
* `http://localhost:4000/chat`
    * websocket chat page
    * `@wolfram question` input
        * Contact WolframAlpha API
* other
    * sign in
    * sign up
    * edit user

## Requirement

* PostgreSQL 9.6
* Java 8
    * Java 9,10: Please add `:jvm-opts ["--add-modules" "java.xml.bind"]` in `project.clj`
* leiningen
* Docker

Note: if running on Docker, PostgreSQL, Java, leiningen is unnecessary

## Usage

### Local

1. create database
    * `postgres=# create database earth_clj_dev;`
1. resolve dependencies
    * `$ lein deps`
1. ClojureScript build
    * `$ lein cljsbuild once`
    * Note: `$ lein cljsbuild auto` is auto build
1. run repl
    * `$ lein repl`
1. generate and run a migration for tables
    * `user=> (migration)`
    * or `$ lein migratus migrate`
1. start server
    * `user=> (go)`
1. please access to http://localhost:4000

Note: if you change database settings, please change `project.clj`

### Docker

1. build image
    * `docker build -t clojure .`
1. start container
    * `docker-compose up -d`
    * please wait...
1. please access to http://localhost:4000


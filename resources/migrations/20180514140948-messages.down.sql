DROP INDEX IF EXSISTS messages_id_idx;
--;;
DROP INDEX IF EXSISTS messages_users_idx;
--;;
DROP SEQUENCE messages_id_seq;
--;;
DROP TABLE messages;

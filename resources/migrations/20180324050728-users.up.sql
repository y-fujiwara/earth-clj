CREATE TABLE IF NOT EXISTS users(
    id SERIAL PRIMARY KEY,
    email character varying not null UNIQUE,
    login_name CHARACTER VARYING not null UNIQUE,
    show_name character VARYING not null,
    password character varying not null,
    bookmark character varying,
    created_at timestamp not null default current_timestamp,
    updated_at timestamp not null default current_timestamp
);
--;;
CREATE INDEX user_email_idx on users(email);
CREATE INDEX user_login_idx on users(login_name);



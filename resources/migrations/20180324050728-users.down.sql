DROP INDEX IF EXSISTS user_email_idx;
--;;
DROP INDEX IF EXSISTS user_login_idx;
--;;
DROP SEQUENCE users_id_seq;
--;;
DROP TABLE users;


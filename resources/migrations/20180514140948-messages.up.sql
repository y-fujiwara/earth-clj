CREATE TABLE IF NOT EXISTS messages(
    id SERIAL PRIMARY KEY,
    message character varying not null,
    user_id integer,
    created_at timestamp not null default current_timestamp,
    updated_at timestamp not null default current_timestamp
);
--;;
CREATE INDEX messages_id_idx on messages(id);
CREATE INDEX messages_users_idx on messages(user_id);


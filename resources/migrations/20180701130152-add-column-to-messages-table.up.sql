ALTER TABLE messages ADD receiver_id integer;
--;;
CREATE INDEX messages_receiver_id_idx on messages(receiver_id);

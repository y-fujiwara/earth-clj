(ns earth-cljs.visualize
  (:require [ajax.core :refer [GET]]
            [earth-cljs.util :as util]
            [cljsjs.chartjs]))

(enable-console-print!)

(def ^:private canvas-elem "<canvas id=\"chart-canvas\">")

(def ^:private chart-option
  {:responsive true
   :scales {:yAxes [{:id "y-axis-temp"
                     :type "linear"
                     :position "left"}
                    {:id "y-axis-hum"
                     :type "linear"
                     :position "right"
                     :gridLines {:drawOnChartArea false}}]}})

(defn- url [city key]
  (str "https://api.openweathermap.org/data/2.5/forecast?q=" city "&APPID=" key))

(defn- build-data [main kind]
  (cond
    (= kind "temperature") (map #(util/calc-temp-without-unit (:temp %)) main)
    (= kind "pressure") (map :pressure main)
    (= kind "humidity") (map :humidity main)
    :else []))

(defn- callback [response]
  (set! (.-innerHTML (util/$ "chart-container")) canvas-elem)
  (let [forecast (:list response)
        main (map :main forecast)
        elem (.getContext (util/$ "chart-canvas") "2d")
        data {:labels   (map #(util/unix-to-time-with-time (:dt %)) forecast)
              :datasets [{:type            "line"
                          :fill false
                          :yAxisID "y-axis-temp"
                          :borderColor     "rgba(254,97,132,0.8)"
                          :label           "temperature"
                          :data            (build-data main "temperature")}
                         {:type "bar"
                          :fill false
                          :yAxisID "y-axis-hum"
                          :borderColor "rgba(54,164,235,0.8)"
                          :backgroundColor "rgba(54,164,235,0.5)"
                          :label "humidity"
                          :data (build-data main "humidity")}]}]
    (js/Chart. elem #js {:type "bar"
                         :data (clj->js data)
                         :options (clj->js chart-option)})))

(defn get-forecast [city key]
  (-> (url city key)
      (GET {:response-format :json
            :keywords?       true
            :handler         callback})))

(ns earth-cljs.map
  (:require [cljsjs.leaflet]
            [earth-cljs.util :as util]))

(defonce ^:private lmap
  (if-let [elem (util/$ "map")]
    (.map js/L "map")
    nil))

(defn- create-overlay [url attr]
  (.tileLayer js/L url #js {:attribution attr :opacity 0.8}))

(defonce ^:private std
  (.tileLayer
   js/L
   "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
   #js {:attribution "&copy; <a href=\"http://osm.org/copyright\" target='_blank'>OpenStreetMap</a> contributors"}))

(defonce ^:private chiriin
  (.tileLayer
   js/L
   "https://cyberjapandata.gsi.go.jp/xyz/std/{z}/{x}/{y}.png"
   #js {:attribution "<a href='https://maps.gsi.go.jp/development/ichiran.html' target='_blank'>地理院タイル</a>"}))

(defn- pf [key]
  (create-overlay
   (str "https://tile.openweathermap.org/map/precipitation_new/{z}/{x}/{y}.png?appid=" key)
   "<a href=\"\">precipitation</a>"))

(defn- pres [key]
  (create-overlay
   (str "https://tile.openweathermap.org/map/pressure_new/{z}/{x}/{y}.png?appid=" key)
   "<a href=\"\">precipitation</a>"))

(defn- wi [key]
  (create-overlay
   (str "https://tile.openweathermap.org/map/wind_new/{z}/{x}/{y}.png?appid=" key)
   "<a href=\"\">wind</a>"))

(defn- te [key]
  (create-overlay
   (str "https://tile.openweathermap.org/map/temp_new/{z}/{x}/{y}.png?appid=" key)
   "<a href=\"\">tempressure</a>"))

(defonce ^:private base-maps
  #js {"Mapbox(osm)" std "Mapbox(chiriin)" chiriin})

(defn- overlay-maps [key]
  #js {"Precipitation" (pf key)
       "Pressure" (pres key)
       "Wind" (wi key)
       "Temperature" (te key)})

(defn- set-control-layer [map-obj base overlay]
  (-> (.-control js/L)
      (.layers base overlay)
      (.addTo map-obj)))

(defn- add-scale [_]
  (-> (.-control js/L)
      (.scale)
      ;; 応急処置っぽい
      (.addTo lmap)))

(defn map-init [id key]
  (-> (.setView lmap #js [37.09 138.52] 16)
      (.addLayer std)
      (set-control-layer base-maps (overlay-maps key))
      (add-scale)))

(defn pan-to [lat lon]
  (.panTo lmap #js [lat lon]))

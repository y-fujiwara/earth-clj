(ns earth-cljs.core
  (:require [earth-cljs.weather :as weather]
            [earth-cljs.map :as map]
            [cljsjs.cropper :as cropper]
            [earth-cljs.visualize :as visual]
            [earth-cljs.weekly :as weekly]
            [earth-cljs.socket :as socket]
            [earth-cljs.util :as util]))
(enable-console-print!)

(defonce ^:private default-city "Tokyo")

(defonce ^:private default-key (.-owmKey js/window))

(defonce ^:private exec-key 13)

(defn- hide [elem]
  (set! (.-display (.-style elem)) "none"))

(defn- visible [elem]
  (set! (.-display (.-style elem)) ""))

(defn- get-input-value [id]
  (-> (util/$ id)
      (.-value)))

(defn- search-event [input-id]
  (fn [_]
    (let [value (get-input-value input-id)]
      (weather/search-exec value default-key map/pan-to)
      (visual/get-forecast value default-key))))

(defn- bookmark-event [input-id]
  (fn [_]
    (-> (get-input-value input-id)
        (weather/bookmark-exec))))

(defn- search-key-event [input-id]
  (fn [e]
    (when (= (.-keyCode e) 13)
      (let [value (get-input-value input-id)]
        (weather/search-exec value default-key map/pan-to)
        (visual/get-forecast value default-key)))))

(defn- scroll-top [_]
  (.scrollTo js/window 0 0))

(defn- index? [agent model]
  (> (.indexOf agent model) 0))

(defn- mobile? [agent]
  (and (index? agent "Mobile")
       (or (index? agent "iPhone")
           (index? agent "iPod")
           (index? agent "Android"))))

(defn- tab? [agent]
  (or (index? agent "iPad")
      (index? agent "Android")))

(defn- get-device []
  (let [ua (.-userAgent js/navigator)]
    (cond
      (mobile? ua) "sp"
      (tab? ua) "tab"
      :else "other")))

(defn- scroll-event []
  (if (< (.-scrollY js/window) 100)
    (do
      (hide (util/$ "page-top"))
      (hide (util/$ "fixed-search")))
    (do
      (visible (util/$ "page-top"))
      (visible (util/$ "fixed-search")))))

(defn- icon-delete []
  (let [data (util/$ "icon-input")]
    ;; エディット画面のアイコンについてはフラグ制御で消すか決める
    (when-let [flag (util/$ "is-icon-delete?")]
      (set! (-> flag .-value) true))
    (set! (-> data .-files) nil)
    (set! (.-src (util/$ "icon-preview")) "")))

(defn- icon-upload [data]
  (let [file-data (first (array-seq (.-files (.-target data))))]
    (if (and (= (.-type file-data) "image/jpeg")
             (> 10485760 (.-size file-data)))
      (do
        (when-let [flag (util/$ "is-icon-delete?")]
          (set! (-> flag .-value) false))
        (set! (.-temp js/window) file-data)
        (let [reader (js/FileReader.)]
          (.readAsDataURL reader file-data)
          (set! (.-onload reader)
                (fn [] (set! (.-src (util/$ "icon-preview")) (.-result reader))))))
      (do
        (js/alert "invalid icon file")
        (icon-delete)))))

(defn init []
  (when-let [upload-elem (util/$ "icon-input")]
    (set! (.-onchange upload-elem) icon-upload))
  (when-let [delete-elem (util/$ "icon-delete")]
    (set! (.-onclick delete-elem) icon-delete))
  (when-let [elem (util/$ "search-city")]
    (weather/get-bookmark (fn [res]
                            (weather/search-exec res default-key map/pan-to)
                            (visual/get-forecast res default-key)))
    (map/map-init "map" default-key)
    (scroll-event)
    (set! (.-onclick (util/$ "page-top")) scroll-top)
    (set! (.-onscroll js/window) scroll-event)
    (when-let [weekly-table (util/$ "weekly-container")]
      (weekly/init weekly-table default-key))
    (when-let [bookmark-elem (util/$ "bookmark-city")]
      (set! (.-onclick bookmark-elem) (bookmark-event "input-city")))
    (set! (.-onclick elem) (search-event "input-city"))
    (set! (.-onclick (util/$ "fixed-search-city")) (search-event "fixed-input-city"))
    (set! (.-onkeypress (util/$ "input-city")) (search-key-event "input-city"))

    (set! (.-onkeypress (util/$ "fixed-input-city")) (search-key-event "fixed-input-city"))))

(set! (.-onload js/window) init)

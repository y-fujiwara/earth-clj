(ns earth-cljs.weekly
  (:require [ajax.core :refer [PUT GET POST]]
            [earth-cljs.util :as util]
            [goog.string :as gstring]
            [cljs.reader :as reader]
            [reagent.core :as reagent]))

(defn- build-weekly-url [city key]
  (str "https://api.openweathermap.org/data/2.5/forecast?q=" city "&appid=" key))

(defn- image-link [weather]
  (str "https://openweathermap.org/img/w/" (gstring/htmlEscape (:icon weather)) ".png"))

(defn- weekly-td [col]
  [:td.weekly-td {:key (str "weekly-td-" col)}
   [:div {:key (str "weeklty-p-" col)}
    [:div
     [:img {:src (image-link (first (:weather col)))}]]
    (let [{temp :temp} (:main col)]
      [:div
       [:span.weekly-max (gstring/htmlEscape (util/calc-temp temp))]])]])

(defn- weekly-body [cols]
  [:tbody
   [:tr
    (for [col cols]
      ^{:key (str "weekly-body-" col)} [weekly-td col])]])

(defn- get-weekly [weekly-url callback]
  (GET weekly-url
       {:response-format :json
        :keywords?       true
        :handler         callback}))

(defn- weekly-table [key]
  (let [state (reagent/atom {:cols []})]
    (letfn [(event-handler
              [val]
              (get-weekly (build-weekly-url val key)
                          #(reset! state {:cols (:list %)})))]
      (with-meta
        (fn []
          [:table#weekly-table.uk-table.uk-table-hover.table-divider
           [:thead
            [:tr
             (for [dt (map :dt (:cols @state))]
               [:th.weekly-th
                {:key (str "weekly-th-" dt)
                 :id (str "weekly-th-" dt)}
                (util/unix-to-time-with-time (gstring/htmlEscape dt))])]]
           [weekly-body (:cols @state)]])
        {:component-did-mount
         (fn [this]
           (set! (.-onchange (util/$ "weekly-city"))
                 event-handler))
         :component-will-unmount
         (fn [this]
           (-> (util/$ "weekly-city")
               (.removeEventListener event-handler)))}))))

(defn init [elem key]
  (reagent/render [(weekly-table key)] elem))

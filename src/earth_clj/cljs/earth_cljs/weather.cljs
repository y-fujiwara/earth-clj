(ns earth-cljs.weather
  (:require [ajax.core :refer [PUT GET POST]]
            [earth-cljs.util :as util]
            [goog.string :as gstring]))

(defn- owm-url [city key]
  (str "https://api.openweathermap.org/data/2.5/weather?q=" city "&appid=" key))

(defonce ^:private ajax-url "/users/bookmark")

(defn- get-edn [res param]
  (get res param))

(defn- image-elem [weather]
  (str "<div><img src=\"https://openweathermap.org/img/w/"
       (gstring/htmlEscape (get-edn weather "icon"))
       ".png\">("
       (gstring/htmlEscape (get-edn weather "description"))
       ")</img></div>"))

(defn- title-text [city]
  (str "Current Weather In " city))

(defn- update-html [elem tag]
  (set! (.-innerHTML elem) tag))

(defn- update-text [elem text]
  (set! (.-textContent elem) text)
  (when-not (nil? (.-onchange elem))
    (.onchange elem text)))

(defn- owm-ajax-handler [callback response]
  (let [weather (first (get-edn response "weather"))
        sys (get-edn response "sys")
        main (get-edn response "main")
        wind (get-edn response "wind")
        clouds (get-edn response "clouds")
        coord (get-edn response "coord")]
    (-> (util/$ "weather")
        (update-html (image-elem weather)))
    (-> (util/$ "city-name")
        (update-text (title-text (gstring/htmlEscape (get-edn response "name")))))
    (-> (util/$ "weekly-city")
        (update-text (gstring/htmlEscape (get-edn response "name"))))
    (-> (util/$ "temperature")
        (update-text (gstring/htmlEscape (util/calc-temp (get-edn main "temp")))))
    (-> (util/$ "sunrise")
        (update-text (util/unix-to-time-full (gstring/htmlEscape (get-edn sys "sunrise")))))
    (-> (util/$ "sunset")
        (update-text (util/unix-to-time-full (gstring/htmlEscape (get-edn sys "sunset")))))
    (-> (util/$ "pressure")
        (update-text (str (gstring/htmlEscape (get-edn main "pressure")) "hpa")))
    (-> (util/$ "humidity")
        (update-text (str (gstring/htmlEscape (get-edn main "humidity")) "%")))
    (-> (util/$ "wind")
        (update-text (str (gstring/htmlEscape (get-edn wind "speed")) "m/s")))
    (-> (util/$ "cloud")
        (update-text (str (gstring/htmlEscape (get-edn clouds "all")) "%")))
    (-> (util/$ "latlon")
        (update-text (str (gstring/htmlEscape (get-edn coord "lat")) " " (gstring/htmlEscape (get-edn coord "lon")))))
    (callback (get-edn coord "lat") (get-edn coord "lon"))))

(defn- set-bookmark-handler [status msg _]
  (.notification js/UIkit msg #js {:status status :timeout "2000"}))

(defn- set-bookmark-wrapper [status msg response]
  (if (= (get-edn response "result") 1)
    (set-bookmark-handler status msg response)
    (set-bookmark-handler "danger" (reduce #(str % "<br />")
                                           (get-edn (get-edn response "errors") "bookmark")) response))) ;; こっちのエラーはわかっていたエラー

(defn- get-bookmark-handler [callback response]
  (callback (get-edn response "bookmark")))

(defn search-exec [city key callback]
  (-> (owm-url city key)
      (GET {:handler (partial owm-ajax-handler callback)})))

(defn bookmark-exec [city]
  (-> ajax-url
      (PUT {:params {:bookmark city}
            :format :raw
            :headers {:x-csrf-token (.-token js/window)}
            :handler (partial set-bookmark-wrapper "success" "Add Bookmark")
            :error-handler (partial set-bookmark-handler "danger" "Failed Bookmark")}))) ;; こっちのエラーはサーバー起因系

(defn get-bookmark [callback]
  (-> ajax-url
      (GET {:handler (partial get-bookmark-handler callback)})))

(ns earth-cljs.socket
  (:require-macros [cljs.core.async.macros :as asyncm :refer (go go-loop)])
  (:require [cljs.core.async :as async :refer (<! >! put! chan)]
            [earth-cljs.util :as util :refer ($)]
            [ajax.core :refer [PUT GET POST]]
            [taoensso.timbre :as timbre :refer-macros (tracef debugf infof warnf errorf)]
            [taoensso.encore :as encore :refer-macros (have have?)]
            [goog.string :as gstring]
            [gravatar.core :as gr]
            [taoensso.sente  :as sente :refer (cb-success?)]))

(defonce ^:private ajax-url "/room")

;;; Add this: --->
(let [{:keys [chsk ch-recv send-fn state]}
      (sente/make-channel-socket! "/chsk" ; Note the same path as before
                                  {:type :auto})] ; e/o #{:auto :ajax :ws}
  (def chsk chsk)
  (def ch-chsk ch-recv) ; ChannelSocket's receive channel
  (def chsk-send! send-fn) ; ChannelSocket's send API fn
  (def chsk-state state)) ; Watchable, read-only atom

;; Sente event handlers

(defn- update-msgs [{:as data :keys [msgs]}]
  (.log js/console msgs)
  (let [output-el ($ "comment-container")]
    (set! (.-innerHTML output-el)
          (reduce #(let [{:keys [icon email name date message]} %2]
                     (str %1
                          "<div class='comment'>"
                          "<a class='avatar'>"
                          "<img src='"
                          (if icon
                            (str "images/icon/" icon)
                            (gr/avatar-url (if email email "") :https true)) "' />"
                          "</a>"
                          "<div class='content'>"
                          "<a class='author'>"
                          (gstring/htmlEscape (if name name "Anonymous"))
                          "</a>"
                          "<div class='metadata'>"
                          "<span class='date'>"
                          (gstring/htmlEscape date)
                          "</span>"
                          "</div>"
                          "<div class='text'>"
                          (gstring/htmlEscape message)
                          "</div>"
                          "</div>"
                          "</div>")) "" msgs))
    (set! (.-scrollTop output-el) (.-scrollHeight output-el))))

(defn- update-users [{:as data :keys [users]}]
  (let [output-el ($ "chat-user-list")]
    (set! (.-innerHTML output-el)
          (str "<span>Chat Users</span>"
               "<ul class='uk-list'>"
               "<li>"
               "<img class='ui avatar image' src='" (gr/avatar-url "" :https true) "' />"
               "<a onclick='selectRoom(\"ALL\", \"ALL\")'>ALL</a>"
               "</li>"
               (reduce #(let [{:keys [email show_name icon id]} %2]
                          (str %1
                               "<li>"
                               "<img class='ui avatar image' src='"
                               (if icon
                                 (str "images/icon/" icon)
                                 (gr/avatar-url (if email email "") :https true))
                               "' />"
                               "<a onclick='selectRoom(" id ",\"" show_name "\")'>" (gstring/htmlEscape show_name) "</a>"
                               "</li>")) "" users)
               "</ul>"))))

(defn- init-msg-handler []
  (chsk-send!
    [:chat/init]
    8000
    (fn [reply]
      (if (sente/cb-success? reply)
        (do
          (update-msgs reply)
          (update-users reply))))))

;; マルチメソッドによるサーバーからのイベント待受
;; :idで判別される
(defmulti -event-msg-handler
  "Multimethod to handle Sente `event-msg`s"
  :id) ; Dispatch on event-id

;; デフォルトメソッド
(defmethod -event-msg-handler
  :default ; Default/fallback case (no other matching handler)
  [{:as ev-msg :keys [event]}]
  (.log js/console (str "Unhandled event: " event)))

(defmethod -event-msg-handler :chsk/state
  [{:as ev-msg :keys [?data]}]
  (let [[old-state-map new-state-map] (have vector? ?data)]
    (if (:first-open? new-state-map)
      (.log js/console (str "Channel socket successfully established!: " new-state-map))
      (.log js/console (str "Channel socket state change: " new-state-map)))))

;; broadcastの受信を行う
(defmethod -event-msg-handler :chsk/recv
  [{:as ev-msg :keys [?data]}]
  (.log js/console (str "data: " ?data))
  (case (first ?data)
    :chat/msgs (update-msgs (second ?data))
    :chat/users (update-users (second ?data))
    (.log js/console (str ?data))))

(defmethod -event-msg-handler :chsk/handshake
  [{:as ev-msg :keys [?data]}]
  (let [[?uid ?csrf-token ?handshake-data] ?data]
    (.log js/console (str "Handshake: " ?data))
    (init-msg-handler)))

(defn event-msg-handler
  "Wraps `-event-msg-handler` with logging, error catching, etc."
  [{:as ev-msg :keys [id ?data event]}]
  (-event-msg-handler ev-msg))

(defn- send-msg-handler [e]
  (let [e ($ "chat-msg")
        v (.-value e)]
    (chsk-send! [:chat/post v])
    (set! (.-value e) "")))

(defonce router_ (atom nil))
(defn stop-router! [] (when-let [stop-f @router_] (stop-f)))
(defn start-router! []
  (stop-router!)
  (reset! router_
          (sente/start-client-chsk-router!
           ch-chsk event-msg-handler)))

(defn toggle-room [id name]
  (-> ajax-url
      (PUT :params {:id id}
            :format :raw
            :headers {:x-csrf-token (.-token js/window)}
            :handler (fn [res]
                       ;; reconnectを呼ぶと自動でinitが呼ばれるのでサーバー側add-watchでブロードキャストするのは危険
                       (sente/chsk-reconnect! chsk)
                       (-> (.offcanvas js/UIkit "#offcanvas") (.hide))
                       (if-let [elem ($ "room")]
                         (set! (.-innerHTML elem) name)))
            :error-handler #(.log js/console (str "chat room toggle error: " %)))))

(when-let [target-el ($ "chat-form")]
  (start-router!))

(when-let [target-el ($ "chat-send")]
  (.addEventListener target-el "click" send-msg-handler))

(.addEventListener js/window "load" (fn []
                                      (toggle-room "ALL" "ALL")))

(set! (.-selectRoom js/window) #(do
                                  (toggle-room %1 %2)
                                  false))


(ns earth-cljs.util
  (:require [cljsjs.moment]))

;; フォーマットも引数にすれば良い
(defn unix-to-time [unixtime]
  (-> (js/moment (* unixtime 1000))
      (.locale "ja")
      (.format "YYYY/MM/DD(ddd)")))

(defn unix-to-time-full [unixtime]
  (-> (js/moment (* unixtime 1000))
      (.locale "ja")
      (.format "YYYY/MM/DD(ddd) HH:mm")))

(defn unix-to-time-with-time [unixtime]
  (-> (js/moment (* unixtime 1000))
      (.locale "ja")
      (.format "M/D(ddd) HH:mm")))

(defn calc-temp [kelvin]
  (-> (- kelvin 273.15)
      (* 10)
      Math/round
      (/ 10)
      (str "°")))

(defn calc-temp-without-unit [kelvin]
  (-> (- kelvin 273.15)
      (* 10)
      Math/round
      (/ 10)))

(defn $ [id]
  (.getElementById js/document id))

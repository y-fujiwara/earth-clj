(ns earth-clj.main
  (:use [org.httpkit.server :only [run-server]])
  (:require [earth-clj.core :as core]
            [migratus.core :as migratus]
            [taoensso.sente :as sente]
            [earth-clj.component :as app]
            [com.stuartsierra.component :as component]
            [earth-clj.db :as db])
  (:gen-class))                                             ;; mainクラスの生成

; 第一引数にthisを取らないとstatic
(defn -main [& {:as args}]
  (let [arg-port (get args "port")
        port (if (string? arg-port) (Integer/parseInt arg-port) arg-port)]
    (component/start (app/create-system :host (get args "host") :port port :join? true))))


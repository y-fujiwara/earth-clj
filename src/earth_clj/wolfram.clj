(ns earth-clj.wolfram
  (:require [clojure.core.async :refer [go-loop <! >!]]
            [ring.util.codec :as codec]
            [earth-clj.socket :refer [ch-chsk]]
            [clojure.xml :as xml]
            [environ.core :refer [env]]
            [clojure.zip :as zip]))

;; TODO: pipeとかpipelineを使ってch-chskとwolfram-chをつなげる

(defonce ^:private app-id (:wolfram-id env))

(defn- make-url [input]
  (println input)
  (str "http://api.wolframalpha.com/v2/query?appid="
       app-id
       "&input="
       (codec/url-encode input)
       "&format=plaintext"))

(defn- xml->data [input]
  (some-> input
          make-url
          xml/parse
          zip/xml-zip
          zip/down
          zip/right
          zip/down
          zip/down
          zip/down
          first))

(defn start-wolfram-service [wolfram-ch]
  (go-loop []
    (when-let [{:as ev-msg :keys [ring-req ?data]} (<! wolfram-ch)]
      (>! (:reader ring-req) (assoc ev-msg :?data
                                           (if-let [ans (xml->data ?data)]
                                             ans
                                             "I have no idea.")))
      (recur))))

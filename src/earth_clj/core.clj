(ns earth-clj.core
  (:use [org.httpkit.server :only [run-server]])
  (:require [bidi.ring :as br :refer (make-handler)]
            [earth-clj.router :as r] 
            [earth-clj.router.dispatcher :as dispatcher]
            [earth-clj.middleware :refer [middleware-set]]))

; 開発時のみ適用されるように制御する関数
(defn- wrap [handler middleware opt]
  (if (= "true" opt)
    (middleware handler)
    (if opt
      (middleware handler opt)
      handler)))

;; qiitaからのパクリ+α
(extend-protocol br/Ring
  clojure.lang.Keyword
  (request [k req _]
    (dispatcher/dispatch k req)))

(def app
  (middleware-set
    (make-handler r/routes)))

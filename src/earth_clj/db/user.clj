(ns earth-clj.db.user
  (:require [clojure.java.jdbc :as jdbc]
            [honeysql.core :as sql]
            [earth-clj.router :as r]
            [honeysql.helpers :as h]
            [crypto.random :as sr]
            [buddy.hashers :as hashers]))

;; TODO: スレッドマクロとかで利用できるようにidでのwhere句とかを外出する
;; メモ (sr/url-part 16)

(defn- login-sql [login-name]
  (sql/format {:select [:id :password]
               :from [:users]
               :where [:= :login_name login-name]}))

(defn- select-sql [id]
  (sql/format {:select [:icon :id :show_name :email :bookmark]
               :from [:users]
               :where [:= :id id]}))

(defn- select-users-sql [ids]
  (sql/format {:select [:id :show_name :email :icon]
               :from [:users]
               :where [:in :id ids]}))

(defn save-user [conn {:keys [email login-name show-name password icon]}]
  #_(new-name (str (java.util.UUID/randomUUID)))
  ;; これで実行すると結果をMapで返してくれる
  (jdbc/db-do-prepared-return-keys
   conn
   (-> (h/insert-into :users)
       ;; valuesを単なるベクタにする場合は以下のようにカラムの指定が必要
       #_(columns :email :login_name :show_name :password)
       (h/values [{:email email
                   :login_name login-name
                   :show_name show-name
                   :icon icon
                   :password (hashers/derive password)}])
       (sql/format))))

(defn login-user [conn login-name password]
  (if-let [user (first (jdbc/query conn (login-sql login-name)))]
    (if (hashers/check password (:password user))
      (:id user)
      false)
    false))

(defn search-user [conn id]
  (jdbc/query conn (select-sql id)))

(defn search-users [conn ids]
  (if (empty? ids)
    []
    (jdbc/query conn (select-users-sql ids))))

(defn delete-user [conn id]
  (jdbc/execute!
   conn
   (-> (h/delete-from :users)
       (h/where [:= :id id])
       (sql/format))))

(defn update-user [conn id {:keys [is-icon-delete? login-name show-name password email icon]}]
  (jdbc/execute!
   conn
   ;; icon削除ではなく、iconファイルがnilの場合は更新しない
   (if (and  (= "false" is-icon-delete?) (nil? icon))
     (-> (h/update :users)
         (h/sset {:login_name login-name
                  :show_name show-name
                  :password (hashers/derive password)
                  :email email})
         (h/where [:= :id id])
         sql/format)
     (-> (h/update :users)
         (h/sset {:login_name login-name
                  :show_name show-name
                  :icon icon
                  :password (hashers/derive password)
                  :email email})
         (h/where [:= :id id])
         sql/format))))

;; bookmark引数は{:bookmark word}になっているので注意
;; Ajaxで直接JSONを送ってきて加工が面倒だったので
(defn update-bookmark [conn id bookmark]
  (jdbc/execute!
   conn
   (-> (h/update :users)
       (h/sset bookmark)
       (h/where [:= :id id])
       sql/format)))

(defn get-user-from-email [conn email]
  (jdbc/query
   conn
   (-> (h/select :id :email :reset_digest :reset_sent_at)
       (h/from :users)
       (h/where [:= :email email])
       sql/format)))

(defn password-reset [conn id]
  (let [token (sr/url-part 16)
        digest (hashers/derive token)
        ret (jdbc/execute!
             conn
             (-> (h/update :users)
                 (h/sset {:reset_digest digest
                          :reset_sent_at (java.sql.Timestamp. (System/currentTimeMillis))})
                 (h/where [:= :id id])
                 sql/format))]
    {:ret (first ret) :path (r/get-path :password-edit :token token)}))

(defn update-password [conn email password]
  (jdbc/execute!
   conn
   (-> (h/update :users)
       (h/sset {:reset_digest nil
                :reset_sent_at nil
                :password (hashers/derive password)})
       (h/where [:= :email email])
       sql/format)))


(ns earth-clj.db.message
  (:require [clojure.java.jdbc :as jdbc]
            [earth-clj.db :as db]
            [honeysql.core :as sql]
            [honeysql.helpers :refer [select from left-join where order-by limit insert-into values]]
            [buddy.hashers :as hashers]))

(defn index-sql []
  (-> (select :message [:show_name "name"] :email [:messages.created_at "date"] :icon)
      (from :messages)
      (left-join :users [:= :messages.user_id :users.id])
      (where [:= :receiver_id nil])
      (order-by :date)
      (limit 100)
      (sql/format)))

(defn add-messages [conn user-id msg]
  (jdbc/execute!
   conn
   (-> (insert-into :messages)
       (values [{:user_id user-id :message msg}])
       (sql/format))))

(defn add-messages-room [conn user-id receiver-id msg]
  (jdbc/execute!
   conn
   (-> (insert-into :messages)
       (values [{:user_id user-id :message msg :receiver_id receiver-id}])
       (sql/format))))

(defn all-messages [conn]
  (jdbc/query conn (index-sql)))

(defn user-messages [conn user-id receiver-id]
  (jdbc/query
   conn
   (-> (select :message [:show_name "name"] :email [:messages.created_at "date"] :icon)
       (from :messages)
       (left-join :users [:= :messages.user_id :users.id])
       (where (if (= user-id receiver-id)
                [:and [:= :receiver_id receiver-id] [:= :user_id user-id]]
                [:or [:and [:= :receiver_id receiver-id] [:= :user_id user-id]]
                 [:and [:= :receiver_id user-id] [:= :user_id receiver-id]]]))
       (order-by :date)
       (limit 100)
       (sql/format))))

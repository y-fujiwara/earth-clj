(ns earth-clj.router
  (:require [bidi.bidi :as b]))

;; メモ (match-route user-routes "/new" :request-method :get)
(def routes 
  ["/" {:get {"" :home
              "chsk" :socket-handshake
              "chat" :chat-index
              "password/" {"reset" :password-reset
                           ["edit/" :token] :password-edit}
              "users/" {"new" :user-new
                        "edit" :user-edit
                        "delete" :user-delete
                        "bookmark" :get-bookmark}}
        :post {"login" :login
               "chsk" :socket-post
               "users/" {"new" :user-create}}
        :put {"room" :room-toggle
              "users/" {"edit" :user-update
                        "bookmark" :set-bookmark}
              "password/" {"reset" :password-token
                           "update" :password-update}}
        :delete {"logout" :logout
                 "users/" {"delete" :user-destroy}}
        true :not-found}])

;; user-routesで定義された内容からパスのみを取得する
;; 関数に紐づけされているパスを逆引きするのでRailsでアクションからパスを引いている感じ
(defn get-path [k & params]
  (if params
    (apply (partial b/path-for routes k) params)
    (b/path-for routes k)))

(defn get-params 
  ([url] (get-params url :get))
  ([url method]
   (let [{:as ret :keys [route-params]} (b/match-route routes url :request-method method)]
     (println ret)
     route-params)))

 ;; いろいろな場所でのリダイレクトに使われるのでここで定義してしまう
(def root-path 
  (b/path-for routes :home))

(ns earth-clj.middleware
  (:require [environ.core :refer [env]]
            [ring.util.response :refer [redirect]]
            [ring.middleware.session :refer [wrap-session]]
            [buddy.auth :refer  [authenticated?]]
            [earth-clj.router :refer [root-path]]
            [taoensso.carmine.ring :refer [carmine-store]]
            [buddy.auth.backends.session :refer [session-backend]]
            [buddy.auth.middleware :refer [wrap-authentication
                                           wrap-authorization]]
            [buddy.auth.accessrules :refer [wrap-access-rules]]
            [earth-clj.middleware.http-response :as http-response]
            [ring.middleware.defaults :as defaults]))

(defn- try-resolve [sym]
  (try
    (require (symbol (namespace sym)))
    (resolve sym)
    (catch java.io.FileNotFoundException _)
    (catch RuntimeException _)))

(defn wrap-dev [handler]
  ; handlerが関数か、varの場合はderefした時に関数かを事前チェック
  {:pre [(or (fn? handler) (and (var? handler) (fn? (deref handler))))]}
  (let [wrap-exceptions (try-resolve 'prone.middleware/wrap-exceptions)
        wrap-reload (try-resolve 'ring.middleware.reload/wrap-reload)]
    (if (and wrap-reload wrap-exceptions)
      (-> handler
          wrap-exceptions
          wrap-reload)
      (throw (RuntimeException. "Middleware requires ring/ring-devel and prone;")))))

(defn wrap-request-method [handler]
  (fn [request]
    (let [params (:params request)
          new-req (case (:_method params)
                    "DELETE" (assoc request :request-method :delete)
                    "PUT" (assoc request :request-method :put)
                    request)]
      (handler new-req)))) 

(def ^:private wrap #'defaults/wrap)

;; environが強制的に文字列にしてくる件と上記で定義されたwarpとの整合性
(def ^:private is-dev? (if (= (:dev env) "true") true false))

;; carmine-storeは単純にringのsessionをcustom-storeにしているだけ
(def custom-site-default
  (if (= "redis" (:session-store env))
    (assoc-in defaults/site-defaults [:session :store] (carmine-store {:spec {:uri (:redis env)} :pool {}} {:expiration-secs (Integer/parseInt (:session-timeout env))})) 
    defaults/site-defaults))

;; TODO: 認証周りは別nsに切り出すべき
(defn unauthorized-handler
  [req meta]
  (redirect root-path))

;; 元々のauthenticated?見たらreqから直接:identityの存在を確認してただけなので参考にして変更
;; -> wrap-defaultsが上手く動いていない原因だったのでカスタムしたら不要になった
(defn session-authenticated? [req]
  (boolean (:identity (:session req))))

(defn middleware-set [handler]
  (let [rules [{:pattern #"(^/users/(?!new$|bookmark$).*$|^/logout.*)" :handler authenticated?}]
        backend (session-backend {:unauthorized-handler unauthorized-handler})]
    (-> handler
        wrap-request-method
        http-response/wrap-http-response
        (wrap wrap-dev is-dev?)
        (wrap-access-rules {:rules rules :policy :allow})
        (wrap-authentication backend)
        (wrap-authorization backend)
        (defaults/wrap-defaults custom-site-default))))

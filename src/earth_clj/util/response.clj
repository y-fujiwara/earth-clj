(ns earth-clj.util.response
  (:require [ring.util.http-response :as res]
            [potemkin :as p]))

; ring.util.response/responseのvarを本ネームスペースに再定義
; response = #'ring.util.response/response
; (def response #'res/response)

; varを再定義するとメタ情報が消えるので設定
; #'response = earth-clj.util.reponse/response
; (alter-meta! #'response #(merge % (meta #'res/response)))

; (def redirect #'res/redirect)

; (alter-meta! #'redirect #(merge % (meta #'res/redirect)))

;; ring-http-responseの全Varをearth-clj.util.responseへ追加するマクロ
;response; コメントアウトしたdefの代わり redirectとかもある
(defmacro import-ns [ns-sym]
  (do
    `(p/import-vars
       [~ns-sym
        ~@(map first (ns-publics ns-sym))])))

(import-ns ring.util.http-response)

(defn html [res]
  (res/content-type res "text/html; charset=utf-8"))

(defn json [res]
  (res/content-type res "application/json; charset=utf-8"))

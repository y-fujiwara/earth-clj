(ns earth-clj.handler.session
  (:require [earth-clj.util.response :as res]
            [earth-clj.router :refer [root-path]]
            [earth-clj.db.user :as user]))

(defn login [{:as req :keys [db params]}]
  (let [username (:login-name params)
        password (:password params)]
    (if-let [id (user/login-user db username password)]
      (-> (res/found root-path)
          (assoc-in [:session :identity] id)
          (assoc-in [:session :uid] {:room #{"ALL"} :id id})
          (assoc :flash {:msg "Login Success"})
          res/html)
      (-> (res/found root-path)
          (assoc :flash {:errors {:login '("Login Failed") }})
          res/html))))

(defn logout [_]
  (-> (res/found root-path)
      (assoc :session nil)
      (assoc :flash {:msg "Logout"})))


(ns earth-clj.handler.password
  (:require [earth-clj.util.response :as res]
            [earth-clj.db.user :as user]
            [clojure.core.async :refer [go >!]]
            [environ.core :refer [env]]
            [buddy.hashers :as hashers]
            [hiccup.page :refer [html5]]
            [earth-clj.util.validation :as uv]
            [bouncer.validators :as v]
            [earth-clj.router :refer [root-path get-path]]
            [earth-clj.view.password :as view]))

(def password-validator {:password [[v/required :message "Please Input Password"]]})

(defn- two-hours-later []
  (-> (java.time.LocalDateTime/now)
      (.minusHours 2)
      (java.sql.Timestamp/valueOf)))

(defn- compare-time [sent]
  (-> sent
      (.after (two-hours-later))))

(defn- mail-body [url] 
  [:alternative
   {:type "text/plain"
    :content (str "To reset your password click the link below:\n\n"
                  url "\n\n"
                  "This link will expire in two hours.\n\n" 
                  "If you did not request your password to be reset, please ignore this email and\n"
                  "your password will stay as it is.")}
   {:type "text/html"
    :content (html5 [:head]
                    [:body
                     [:div
                      [:h1 "Password reset"]
                      [:p "To reset your password lick the link below:"]
                      [:a {:href url} "Reset password"]
                      [:p "This link will expire in two hours."]
                      [:p "If you did not request your password to be reset, please ignore this email and your password will stay as it is."]]])}])

(defn reset [req]
  (-> (view/reset-view req)
      res/ok
      res/html))

;; ringの中身でrequestからURLを組み立てる関数が似たような感じだったのでパクリ
(defn build-full-url [email path {:keys [scheme server-name server-port]}]
  (str (name scheme) 
       "://" server-name 
       (if (= server-port 80) 
         ""
         (str ":" server-port))
       path
       "?email=" email))

(defn set-token [{:as req :keys [db mailer params]}]
  (if-let [{:as data :keys [id email]} (first (user/get-user-from-email db (:email params)))] 
    (let [{:keys [ret path]} (user/password-reset db id)
          url (build-full-url email path req)]
      (if (pos? ret)
        (do
          (println (str "reset url: " url))
          (go 
            (>! mailer {:body (mail-body url)
                        :subject "【earth-clj】Password reset"
                        :to email
                        :from (:mail-from env)}))
          (-> (res/found root-path)
              (assoc :flash {:msg "Email sent with password reset instructions"})
              res/html))
        (res/conflict!)))
    (-> (res/found (get-path :password-reset))
        (assoc :flash {:errors {:password '("Email Not Found")}})
        res/html)))

(defn edit [{:as req :keys [db params uri]}]
  (let [{:keys [email token]} params]
    (if-let [{:as data :keys [id reset_digest reset_sent_at]} (first (user/get-user-from-email db email))]
      (if (hashers/check token reset_digest)
        (if (compare-time reset_sent_at)
          (-> (view/edit-view req)
              res/ok
              res/html) 
          (-> (res/found root-path)
              (assoc :flash {:errors {:password-edit '("Password reset has expired")}})
              res/html))
        (-> (res/found root-path)
            (assoc :flash {:errors {:password-edit '("Invalid Reset Token")}})
            res/html))
      (-> (res/found root-path)
          (assoc :flash {:errors {:password-edit '("Email Not Found")}})
          res/html))))

(defn update [{:as ret :keys [db params]}]
  (let [{:keys [password confirm-password email token]} params]
    (if (= password confirm-password)
      (uv/with-fallback #(-> (res/found (str (get-path :password-edit :token token) "?email=" email))
                             (assoc :flash {:errors %})
                             res/html)
        (let [params (uv/validate params password-validator)]
          (if (pos? (first (user/update-password db email password)))
            (-> (res/found root-path)
                (assoc :flash {:msg "Success Password Update"})
                res/html)
            (res/conflict!))))
      (-> (res/found (str (get-path :password-edit :token token) "?email=" email))
          (assoc :flash {:errors {:password-update '("Password confirmation doesn't match Password")}})
          res/html))))


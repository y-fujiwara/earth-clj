(ns earth-clj.handler.user
  (:require [earth-clj.db.user :as user]
            [clojure.data.json :as json]
            [earth-clj.router :refer [get-path root-path]]
            [clojure.string :refer [lower-case]]
            [clojure.java.io :as io]
            [bouncer.validators :as v]
            [earth-clj.util.response :as res]
            [earth-clj.util.validation :as uv]
            [earth-clj.view.user :as view]))

(defonce ^:private default-bookmark "Tokyo")
(defonce ^:private file-extensions #".+\.(png|jpg|jpeg|gif|JPG|JPEG|PNG|GIF)")
(defonce ^:private resource-path "resources/public/images/icon/")

(defn- less-10mb? [{:keys [size]}]
  (if (nil? size)
    true
    (> 10485760 size)))

(defn- is-image? [{:keys [filename]}]
  (if (or (empty? filename) (nil? filename))
    true
    (not (nil? (re-find file-extensions (lower-case filename))))))

(def user-validator {:login-name [[v/required :message "Please Input Login Id"]]
                     :show-name  [[v/required :message "Please Input Show Name"]]
                     :password   [[v/required :message "Please Input Password"]]
                     :icon       [[less-10mb? :message "icon file is too big"]
                                  [is-image?  :message "icon file is not image"]]
                     :email      [[v/required :message "Please Input Email"]
                                  [v/email :message "Invalid Email"]]})

(def bookmark-validator {:bookmark [[v/required :message "Please Input Bookmark"]]})

;; アップロードされたファイルを保存する
(defn- save-file [filename tempfile]
  (io/copy tempfile (io/file (str resource-path filename)))
  (io/delete-file tempfile))

(defn user-new [{:as req :keys [params]}]
  (-> (view/user-new-view req)
      res/ok
      res/html))

;; TODO:bouncerで独自バリデーションを掛ける方法を探したほうが良い
(defn user-create [{:as req :keys [db params]}]
  (let [file (-> params :icon :tempfile)
        size (-> params :icon :size)]
    (uv/with-fallback #(-> (res/found (get-path :user-new))
                           (assoc :flash {:errors %})
                           res/html)
      (let [params (uv/validate params user-validator)
            icon-name (if (zero? size) nil (str (java.util.UUID/randomUUID)))]
        (if-let [data (user/save-user db (assoc params :icon icon-name))]
          (do
            (when (not (nil? icon-name)) (save-file icon-name file)) ;; ファイルがアップロードされてない場合は保存しない
            (-> (res/found root-path)
                (assoc-in [:session :identity] (:id data))
                (assoc-in [:session :uid] {:room #{"ALL"} :id (:id data)})
                (assoc :flash {:msg "Success Create User"})
                res/html))
          (res/internal-server-error!))))))

(defn user-edit [{:as req :keys [db params]}]
  (-> (view/user-edit-view req (first (user/search-user db (:identity (:session req)))))
      res/ok
      res/html))

(defn user-update [{:as req :keys [db params]}]
  (let [file (-> params :icon :tempfile)
        size (-> params :icon :size)]
    (uv/with-fallback #(-> (res/found (get-path :user-edit))
                           (assoc :flash {:errors %})
                           res/html)
      (let [params (uv/validate params user-validator)
            icon-name (if (zero? size) nil (str (java.util.UUID/randomUUID)))]
        (if (pos? (first (user/update-user db (:identity (:session req)) (assoc params :icon icon-name))))
          (do
            (when (not (nil? icon-name)) (save-file icon-name file))
            (-> (res/found root-path)
                (assoc :flash {:msg "Success Edit User"})
                res/html))
          (res/conflict!))))))

(defn user-delete [{:as req :keys [params]}]
  (-> (view/user-delete-view req)
      res/ok
      res/html))

(defn user-destroy [{:as req :keys [db params]}]
  ;; ログインと同じロジックを使って消そうとしているユーザー情報が正しいか確認する
  (let [username (:login-name params)
        password (:password params)]
    (if-let [id (user/login-user db username password)]
      (if (pos? (first (user/delete-user db id)))
        (-> (res/found root-path)
            (assoc :flash {:msg "Deleted User"})
            (assoc :session {})
            res/html)
        (res/conflict!))
      (-> (res/found (get-path :user-delete))
          (assoc :flash {:errors {:delete-confirm '("Invalid User Info")}})
          res/html))))

(defn- select-bookmark [db id]
  (if (nil? id)
    default-bookmark
    (if-let [bookmark (:bookmark (first (user/search-user db id)))]
      bookmark
      default-bookmark)))

(defn set-bookmark [{:as req :keys [db params]}]
  (uv/with-fallback #(-> {:result 0 :errors %}
                         json/write-str
                         res/ok
                         res/json)
                    (let [params (uv/validate params bookmark-validator)]
                      (if (pos? (first (user/update-bookmark db (:identity (:session req)) params)))
                        (-> {:result 1 :msg "Success Set Bookmark"}
                            json/write-str
                            res/ok
                            res/json)
                        (res/conflict!)))))

(defn get-bookmark [{:as req :keys [db params]}]
  (-> {:bookmark (select-bookmark db (:identity (:session req)))}
      json/write-str
      res/ok
      res/json))


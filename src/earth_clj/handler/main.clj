(ns earth-clj.handler.main
  (:require [earth-clj.util.response :as res]
            [earth-clj.view.main :as view]))

(defn home [req]
  (-> (view/home-view req)
      res/ok
      res/html))


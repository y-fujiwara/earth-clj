(ns earth-clj.handler.chat
  (:require [clojure.data.json :as json]
            [earth-clj.view.chat :as view]
            [earth-clj.util.response :as res]
            [earth-clj.socket :as socket]
            [bouncer.validators :as v]))

(defn chat-index [{:as req :keys [params]}]
  ;; 今ページに入ったときのみ開始する
  (-> req
      (assoc-in [:session :uid :room] #{"ALL"})
      view/index-view 
      res/ok
      res/html))

(defn room-toggle [{:as req :keys [params]}]
  {:status 200 :session
   (let [session (:session req)
         receiver-id (:id params)
         uid (:uid session)
         id (:id uid)]
     (if (or (= receiver-id "ALL") (= id receiver-id))
       (assoc session :uid {:room #{receiver-id} :id id})
       (if (= id (Integer/parseInt receiver-id))
        (assoc session :uid {:room #{id} :id id})
        (assoc session :uid {:room #{(Integer/parseInt receiver-id) id} :id id}))))})

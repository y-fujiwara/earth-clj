(ns earth-clj.socket
  (:require [taoensso.sente :as sente]
            [clojure.core.async :refer [go-loop go <! >!]]
            [earth-clj.db.message :as message]
            [earth-clj.db.user :as user]
            [clojure.string :refer [trim]]
            [taoensso.timbre    :as timbre :refer (tracef debugf infof warnf errorf)]
            [taoensso.sente.server-adapters.http-kit :refer (get-sch-adapter)]))

;; 正規表現
(def ^:private prefix #"(^@wolfram)(.*)")

;; sente用の設定 要確認
(let [{:keys [ch-recv send-fn connected-uids
              ajax-post-fn ajax-get-or-ws-handshake-fn]}
      (sente/make-channel-socket! (get-sch-adapter) {})]
  (def ring-ajax-post ajax-post-fn)
  (def ring-ajax-get-or-ws-handshake ajax-get-or-ws-handshake-fn)
  (def ch-chsk ch-recv) ; ChannelSocket's receive channel
  (def chsk-send! send-fn) ; ChannelSocket's send API fn
  (def connected-uids connected-uids)) ; Watchable, read-only atom

(defn toggle-handler [])

;; 全UIDに対してメッセージをブロードキャストする
(defn msgs-broadcast [db]
  (debugf "BroadCastMsgs")
  ;; ログインするとこのUIDに色々つけるが、それに対してプッシュしているだけ
  (let [msgs (message/all-messages db)]
    (doseq [{:as uid :keys [id room]} (:any @connected-uids)]
      (if (or (= uid :taoensso.sente/nil-uid) (= (first room) "ALL"))
        (chsk-send! uid [:chat/msgs {:msgs msgs}])
        (if-let [s (second room)]
          ;; こっちは基本的にあまりなし。ログイン直後にどっかのルームに入っているとかの場合のみ
          (chsk-send! uid [:chat/msgs {:msgs (message/user-messages db (first room) s)}])
          (chsk-send! uid [:chat/msgs {:msgs (message/user-messages db (first room) (first room))}]))))))

(defn msgs-rooompush [db session-room]
  (debugf "RoomPushMsgs")
  (let [f (first session-room)
        s (second session-room)
        msgs (message/user-messages db f (if (nil? s) f s))]
    (doseq [{:as uid :keys [id room]} (:any @connected-uids)]
      (when (= room session-room)
        (chsk-send! uid [:chat/msgs {:msgs msgs}])))))

(defn get-users [db]
  (let [uids (filter
             #(not= :taoensso.sente/nil-uid %)
             (:any @connected-uids))
        ids (map :id uids)]
    (user/search-users db ids)))

(defn users-broadcast [db]
  (let [users (get-users db)]
    (doseq [uid (:any @connected-uids)]
      (chsk-send! uid [:chat/users {:users users}]))))

(defn add-msg-in-db [ring-req data]
  (let [{:as uid :keys [room id]} (:uid (:session ring-req))]
    (if (= "ALL" (first room))
      (do
        (message/add-messages (:db ring-req) (:identity (:session ring-req)) data)
        (msgs-broadcast (:db ring-req))) 
      (do
        (if-let [another (first (disj room id))]
          (message/add-messages-room (:db ring-req) (:identity (:session ring-req)) another data)  
          (message/add-messages-room (:db ring-req) (:identity (:session ring-req)) id data))
        (msgs-rooompush (:db ring-req) room)))))

;; イベントを送られてきたIDによって分岐するマルチメソッド
(defmulti -event-msg-handler
          "Multimethod to handle Sente `event-msg`s"
          :id)

;; :idが何にもマッチしなかった場合
(defmethod -event-msg-handler
  :default
  [{:as ev-msg :keys [event id ?data ring-req ?reply-fn send-fn]}]
  (let [session (:session ring-req)
        uid     (:uid     session)]
    (debugf "Unhandled event: %s" event)
    (when ?reply-fn
      (?reply-fn {:umatched-event-as-echoed-from-from-server event}))))

(defmethod -event-msg-handler
  :chat/init
  [{:as ev-msg :keys [event id ?data ring-req ?reply-fn send-fn]}]
  (let [session (:session ring-req)
        uid (:uid session)
        room (:room uid)
        id (:id uid)
        db (:db ring-req)
        msgs (if (= "ALL" (first room))
               (message/all-messages db)
               (if-let [s (second room)]
                 (message/user-messages db (first room) s)
                 (message/user-messages db (first room) (first room))))]
    (debugf "Init event: %s" event)
    (when ?reply-fn
      (?reply-fn {:msgs msgs
                  :users (if (= uid :taoensso.sente/nil-uid) [] (get-users db))}))))

(defmethod -event-msg-handler
  :chat/post
  [{:as ev-msg :keys [event id ?data ring-req ?reply-fn send-fn]}]
  ;; nthでも良いけどIndex~の例外が出るのでlastにする
  ;; 正規表現的に最大でも3つの要素のvectorになる
  (when-let [msg (last (re-find prefix ?data))]
    (go
      (>! (:qasystem ring-req) (assoc ev-msg :?data msg))))
  (add-msg-in-db ring-req ?data))

;; Wolframから返されるものを待ち受ける
(defn watch-wolfram-service [watch-ch]
  (go-loop []
    (when-let [{:as ev-msg :keys [ring-req ?data]} (<! watch-ch)]
      (add-msg-in-db ring-req ?data)
      (recur))))

;; イベントハンドラ発火元関数
(defn event-msg-handler
  "Wraps `-event-msg-handler` with logging, error catching, etc."
  [{:as ev-msg :keys [id ?data event]}]
  (-event-msg-handler ev-msg) ; Handle event-msgs on a single thread
  ;; Handle event-msgs on a thread pool
  #_(future (-event-msg-handler ev-msg)))

;; add-watchはref/atom/varの切り替わりを監視する組み込み関数

(add-watch connected-uids :connected-uids
           (fn [a b old new]
             (when (not= old new)
               #_(users-broadcast db)
               (infof "Connected uids change: %s" @connected-uids))))
 

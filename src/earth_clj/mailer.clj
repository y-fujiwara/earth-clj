(ns earth-clj.mailer
  (:require [postal.core :refer [send-message]]
            [clojure.core.async :refer [go-loop <! >!]]))

(defn start-mailer [mail-spec mailer-ch]
  (go-loop []
    (when-let [{:as data :keys [body subject to from]} (<! mailer-ch)]
      (try
        (send-message mail-spec {:from from
                                 :to to
                                 :subject subject
                                 :body body})
        (catch Exception e (println (str "Caught Mailer Exception: " (.getMessage e)))))
      (recur))))

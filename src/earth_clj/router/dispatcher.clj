(ns earth-clj.router.dispatcher
  (:require [earth-clj.util.response :as res]
            [earth-clj.handler.main :as main]
            [earth-clj.handler.session :as session]
            [earth-clj.handler.user :as user]
            [earth-clj.handler.password :as password]
            [earth-clj.socket :as socket]
            [earth-clj.handler.chat :as chat]))

(defmulti dispatch (fn [k req] k))

(defmethod dispatch :default [k req]
  (res/not-found!))

(defmethod dispatch :not-found [k req]
  (res/not-found!))

(defmethod dispatch :home [k req]
  (main/home req))

(defmethod dispatch :chat-index [k req]
  (chat/chat-index req))

(defmethod dispatch :login [k req]
  (session/login req))

(defmethod dispatch :logout [k req]
  (session/logout req))

(defmethod dispatch :user-new [k req]
  (user/user-new req))

(defmethod dispatch :user-create [k req]
  (user/user-create req))

(defmethod dispatch :user-edit [k req]
  (user/user-edit req))

(defmethod dispatch :user-update [k req]
  (user/user-update req))

(defmethod dispatch :user-delete [k req]
  (user/user-delete req))

(defmethod dispatch :user-destroy [k req]
  (user/user-destroy req))

(defmethod dispatch :get-bookmark [k req]
  (user/get-bookmark req))

(defmethod dispatch :set-bookmark [k req]
  (user/set-bookmark req))

(defmethod dispatch :socket-handshake [k req]
  (socket/ring-ajax-get-or-ws-handshake req))

(defmethod dispatch :socket-post [k req]
  (socket/ring-ajax-post req))

(defmethod dispatch :password-reset [k req]
  (password/reset req))

(defmethod dispatch :password-token [k req]
  (password/set-token req))

(defmethod dispatch :password-edit [k req]
  (password/edit req))

(defmethod dispatch :password-update [k req]
  (password/update req))

(defmethod dispatch :room-toggle [k req]
  (chat/room-toggle req))

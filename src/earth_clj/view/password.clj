(ns earth-clj.view.password
  (:require [earth-clj.view.layout :as layout]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [earth-clj.router :refer [get-path]]
            [hiccup.core :refer [h]]
            [hiccup.form :as hf]))
 
(defn reset-view [req]
  (->> [:main#main
        [:div.uk-flex.uk-margin.uk-card.uk-card-default.uk-card-body.card-password-reset
         (hf/form-to {:class "password-reset"}
                     [:put (get-path :password-token)]
                     (anti-forgery-field)
                     [:fieldset.uk-fieldset
                      [:legend.uk-legend "Forgot Password"]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "email"} "EMAIL"]
                       [:div.uk-form-controls
                        (hf/email-field {:class "uk-input" :id "email" :placeholder "Input Mail Addr"} "email")]]
                      [:div.uk-text-right
                       (hf/submit-button {:class "uk-button uk-button-primary"} "SEND")]])]]
       (layout/common req "Password Reset")))

(defn edit-view [req]
  (->> [:main#main
        [:div.uk-flex.uk-margin.uk-card.uk-card-default.uk-card-body.card-password-reset
         (hf/form-to {:class "password-reset"}
                     [:put (get-path :password-update)]
                     (anti-forgery-field)
                     (hf/hidden-field {:id "email"} "email" (:email (:params req)))
                     (hf/hidden-field {:id "token"} "token" (:token (:params req)))
                     [:fieldset.uk-fieldset
                      [:legend.uk-legend "Password Edit"]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "create-password"} "PASSWORD"]
                       [:div.uk-form-controls
                        (hf/password-field {:class "uk-input" :id "create-password" :placeholder "Input Password"} "password")]]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "confirm-password"} "CONFIRM PASSWORD"]
                       [:div.uk-form-controls
                        (hf/password-field {:class "uk-input" :id "confirm-password" :placeholder "Input Confirm Password"} "confirm-password")]]
                      [:div.uk-text-right
                       (hf/submit-button {:class "uk-button uk-button-default"} "UPDATE")]])]]
       (layout/common req "Password Edit")))


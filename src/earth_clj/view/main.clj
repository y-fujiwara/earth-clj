(ns earth-clj.view.main
  (:require [earth-clj.view.layout :as layout]
            [buddy.auth :refer [authenticated?]]))

(def ^:private cols ["weather" "temperature" "wind" "cloud" "pressure"
                     "humidity" "sunrise" "sunset" "latlon"])

(defn- create-weekly [tag]
  (for [_ [0 1 2 3 4 5 6 7]]
    [tag]))

(defn home-view [req]
  ; ->>は最後の引数に適用していくパイプライン
  (->> [:main#main
        [:div.uk-flex.uk-margin
         [:input#input-city.uk-input.input-city {:type "text" :placeholder "Enter Place Name"}]
         [:button#search-city.uk-button.uk-button-primary "SEARCH"]
         (if (authenticated? req)
           [:button#bookmark-city.uk-button.uk-button-default.uk-margin-small-left "BOOKMARK"])]
        [:div#weekly
         [:div
          [:h3.uk-heading-line
           [:span "Weekly Weather In "
            [:span#weekly-city ""]]]
          [:div#weekly-container.uk-overflow-auto]]]
        [:div#chart-container {:style "width: 100%"}
         [:canvas#chart-canvas]]
        [:div#contents {:class "uk-child-width-expand@s" :uk-grid ""}
         [:div.represent
          [:h3.uk-heading-line
           [:span#city-name "Current Weather In"]]
          [:div.uk-overflow-auto.represent-contents
           [:table.uk-table.uk-table-striped
            [:thead
             [:tr
              [:th "ITEM"]
              [:th "DATA"]]]
            [:tbody
             (for [col cols]
               [:tr
                [:td (clojure.string/upper-case col)]
                [:td {:id col}]])]]]]
         [:div.visual
          [:div#map-contents
           [:div#map]]]]
        [:div#fixed-search.fixed-item {:style "display: none;"}
         [:input#fixed-input-city.uk-input.uk-form-width.small.input-city.input-city-header {:type "text" :placeholder "Enter Place"}]
         [:button#fixed-search-city.uk-button.uk-button-primary.search-header "SEARCH"]]
        [:p#page-top.fixed-item {:style "display: none;"}
         [:a {:uk-icon "icon: arrow-up"}]]]
       (layout/common req "MAIN")))

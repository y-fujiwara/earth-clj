(ns earth-clj.view.layout
  (:require [hiccup.page :refer [html5 include-css include-js]]
            [hiccup.core :refer [h]]
            [earth-clj.db.user :refer [search-user]]
            [environ.core :refer [env]]
            [earth-clj.router :as router]
            [buddy.auth :refer  [authenticated?]]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]
            [hiccup.form :as hf]))

(defn error-messages [req]
  (when-let [errors (:errors req)]
    (for [[k v] errors
          msg v]
      [:div.uk-alert-danger {:uk-alert ""}
       [:a.uk-alert-close {:uk-close ""}]
       [:p msg]])))

(defn flash-message [req]
  (when-let [flash (:flash req)]
    [:div.flash-msg
     (if-let [primary (:msg flash)]
       [:div.uk-alert-primary {:uk-alert ""}
        [:a.uk-alert-close {:uk-close ""}]
        [:p primary]]
       (error-messages flash))]))

(defn common [req title & body]
  ; html5マクロはhiccup.core/htmlをラップしているので同じ様に使える
  (html5
    [:head
     [:title (str "Earth | " title)]
     [:meta {:name "viewport" :content "width=device-width" :initial-scale 1.0 :maximum-scale 1.0 :minimum-scale 1.0}]
     [:meta {:http-equiv "content-language" :content "ja"}]
     (include-css "/css/semantic.min.css"
                  "https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.40/css/uikit.min.css"
                  "https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
                  "/css/style.css")
     (include-js "https://code.jquery.com/jquery-3.1.1.min.js"
                 "/js/semantic.min.js"
                 "https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.40/js/uikit.min.js"
                 "https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.40/js/uikit-icons.min.js")]
    [:body
     [:div#main-contents
      [:header.uk-navbar-container.tm-navbar-container.uk-sticky.uk-sticky-fixed.uk-active {:uk-sticky "media: true"}
       [:div.uk-container-expand
        [:nav.uk-navbar
         [:div.uk-navbar-left
          [:a.uk-navbar-toggle {:uk-navbar-toggle-icon "" :uk-toggle "target: #offcanvas"}]
          [:a.uk-navbar-item.uk-logo {:href router/root-path} "Earth"]]
         (if (authenticated? req)
           [:div.uk-margin.uk-navbar-right
            [:ul.uk-navbar-nav
             [:li
              (hf/form-to
                [:post (router/get-path :logout)]
                ;; 以下2つはidがユニークじゃないとか怒られるのでログアウト用にベタに作る
                (hf/hidden-field {:id nil} "__anti-forgery-token" *anti-forgery-token*)
                (hf/hidden-field {:id nil} "_method" "DELETE")
                (error-messages req)
                [:button.uk-button.uk-button-default.logout
                 {:type "submit"} "LOGOUT"])]]])]]]
      (flash-message req)
      (error-messages req)
      body]
     [:div#offcanvas {:uk-offcanvas "overlay: true"}
      [:div.uk-offcanvas-bar
       [:h3 "MENU"]
       (if (authenticated? req)
         [:ul.uk-nav.uk-nav-default
          [:li.uk-nav-header "LOGGED IN NAME"]
          [:li
           [:p
            [:span.uk-margin-small-right {:uk-icon "icon: user"}]
            (let [id (:identity (:session req))]
              ;; TODO: 切り分けるならhandlerでやるべき
              [:span (h (get (first (search-user (:db req) id)) :show_name))])]]
          [:li.uk-nav-divider]
          [:li.uk-nav-header "CHAT"]
          [:li
           [:a {:href (router/get-path :chat-index)}
            [:span.uk-margin-small-right {:uk-icon "icon: file-edit"}] "CHAT"]]
          [:li.uk-nav-divider]
          [:li.uk-nav-header "USER SETTINGS"]
          [:li
           [:a {:href (router/get-path :user-edit)}
            [:span.uk-margin-small-right {:uk-icon "icon: file-edit"}] "USER EDIT"]]
          [:li
           [:a {:href (router/get-path :user-delete)}
            [:span.uk-margin-small-right {:uk-icon "icon: ban"}] "USER DELETE"]]
          [:li.uk-nav-divider]]
         [:ul.uk-nav.uk-nav-default                         ;; LOGINしてない場合
          [:li.uk-nav-header "CHAT"]
          [:li
           [:a {:href (router/get-path :chat-index)}
            [:span.uk-margin-small-right {:uk-icon "icon: file-edit"}] "CHAT"]]
          [:li.uk-nav-divider]
          [:li
           [:a {:href "#" :uk-toggle "target: #modal-login"}
            [:span.uk-margin-small-right {:uk-icon "icon: sign-in"}] "SIGN IN"]
           [:a {:href (router/get-path :user-new)}
            [:span.uk-margin-small-right {:uk-icon "icon: user"}] "SIGN UP"]]])
       [:div#chat-user-list.chat-user-list]
       [:button.uk-button.uk-button-default.uk-offcanvas-close.uk-width-1-1.uk-margin {:type "button"} "Close"]]]
     [:script (str "window.token = '" *anti-forgery-token* "';\n"
                   "window.owmKey = '" (:owm-key env) "';")]
     [:div#modal-login {:uk-modal ""}
      [:div.uk-modal-dialog.uk-modal-body
       [:h2.uk-modal-title "LOGIN FORM"]
       [:div.uk-flex.uk-margin
        (hf/form-to {:class "login-form"}
                    [:post (router/get-path :login)]
                    (hf/hidden-field {:id nil} "__anti-forgery-token" *anti-forgery-token*)
                    (error-messages req)
                    [:div.uk-margin
                     [:label.uk-form-label {:for "login-name"} "LOGIN ID"]
                     [:div.uk-form-controls
                      (hf/text-field {:class "uk-input" :id "login-name" :placeholder "Input Login Id"} "login-name")]]
                    [:div.uk-margin
                     [:label.uk-form-label {:for "password"} "PASSWORD"]
                     [:div.uk-form-controls
                      (hf/password-field {:class "uk-input" :id "password" :placeholder "Input Password"} "password")]]
                    [:div
                     [:div.uk-text-left
                      [:a {:href (router/get-path :password-reset)} "Forgot Your Password?"]]
                     [:div.uk-text-right
                      [:button.uk-button.uk-button-default.uk-modal-close.uk-margin-right
                       {:type "button"} "Cancel"]
                      [:button.uk-button.uk-button-primary {:type "submit"} "LOGIN"]]])]]]
     (include-js "/js/main.js")]))


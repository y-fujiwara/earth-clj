(ns earth-clj.view.chat
  (:require [earth-clj.view.layout :as layout]
            [hiccup.page :refer [include-css include-js]]))

(defn index-view [req]
  (->> [:main#main.chat-main
        [:div.ui.comments
         [:h3.ui.dividing.header "Chat"
          [:div#room.ui.sub.header "ALL"]]
         [:div#comment-container]
         [:form#chat-form.ui.reply.form
          [:div.field
           [:textarea#chat-msg]
           [:div#chat-send.ui.blue.labeled.submit.icon.button "Add Reply"
            [:i.icon.edit]]]]
         [:div#msg-test]]]
       (layout/common req "Chat")))

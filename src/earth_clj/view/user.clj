(ns earth-clj.view.user
  (:require [earth-clj.view.layout :as layout]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [earth-clj.router :refer [get-path]]
            [hiccup.core :refer [h]]
            [hiccup.form :as hf]))

(defn user-new-view [req]
  (->> [:main#main
        [:div.uk-flex.uk-margin.uk-card.uk-card-default.uk-card-body.card-user-register
         (hf/form-to {:class "user-register" :enctype "multipart/form-data"}
                     [:post (get-path :user-create)]
                     (anti-forgery-field)
                     [:fieldset.uk-fieldset
                      [:legend.uk-legend "USER REGISTRATION"]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "show-name"} "SHOW NAME"]
                       [:div.uk-form-controls
                        (hf/text-field {:class "uk-input" :id "show-name" :placeholder "Input Show Name"} "show-name")]]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "email"} "EMAIL"]
                       [:div.uk-form-controls
                        (hf/email-field {:class "uk-input" :id "email" :placeholder "Input Mail Addr"} "email")]]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "create-login-name"} "LOGIN ID"]
                       [:div.uk-form-controls
                        (hf/text-field {:class "uk-input" :id "create-login-name" :placeholder "Input Login Id"} "login-name")]
                       [:img#select-image.select-icon]]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "icon"} "UPLOAD ICON(10MB)"]
                       [:div.uk-form-controls
                        [:div.js-upload {:uk-form-custom ""}
                         (hf/file-upload {:id "icon-input" :accept ".jpg, .jpeg, .png, .gif"} "icon")
                         [:button.uk-button.uk-button-default "Upload"]]]]
                      [:div.uk-margin
                       [:label.uk-form-label "DELETE ICON"]
                       [:div.uk-form-controls
                        [:button#icon-delete.uk-button.uk-button-default {:type :button} "Delete"]]]
                      [ :div.uk-margin
                       [:label.uk-form-label "PREVIEW ICON"]
                       [:div.uk-form-controls
                        [:img#icon-preview {:style "width:80px;height:80px"}]]]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "create-password"} "PASSWORD"]
                       [:div.uk-form-controls
                        (hf/password-field {:class "uk-input" :id "create-password" :placeholder "Input Password"} "password")]]
                      [:div.uk-text-right
                       (hf/submit-button {:class "uk-button uk-button-default"} "CREATE")]])]]
       (layout/common req "Create User")))

(defn user-edit-view [req current-user]
  (->> [:main#main
        [:div.uk-flex.uk-margin.uk-card.uk-card-default.uk-card-body.card-user-register
         (hf/form-to {:class "user-register" :enctype "multipart/form-data"}
                     [:put (get-path :user-update)]
                     (anti-forgery-field)
                     [:fieldset.uk-fieldset
                      [:legend.uk-legend "USER EDIT"]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "show-name"} "SHOW NAME"]
                       [:div.uk-form-controls
                        (hf/text-field
                          {:class "uk-input" :id "show-name" :placeholder "Input Show Name"}
                          "show-name"
                          (h (:show_name current-user)))]]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "email"} "EMAIL"]
                       [:div.uk-form-controls
                        (hf/email-field
                          {:class "uk-input" :id "email" :placeholder "Input Mail Addr"}
                          "email"
                          (h (:email current-user)))]]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "edit-login-name"} "LOGIN ID"]
                       [:div.uk-form-controls
                        (hf/text-field
                          {:class "uk-input" :id "edit-login-name" :placeholder "Input Login Id"}
                          "login-name"
                          (h (:login_name current-user)))]]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "icon"} "UPLOAD ICON(10MB)"]
                       [:div.uk-form-controls
                        [:div.js-upload {:uk-form-custom ""}
                         (hf/file-upload {:id "icon-input" :accept ".jpg, .jpeg, .png, .gif"} "icon")
                         [:button.uk-button.uk-button-default "Upload"]]]]
                      [:div.uk-margin
                       [:label.uk-form-label "DELETE ICON"]
                       [:div.uk-form-controls
                        [:button#icon-delete.uk-button.uk-button-default {:type :button} "Delete"]]]
                      [ :div.uk-margin
                       [:label.uk-form-label "PREVIEW ICON"]
                       [:div.uk-form-controls
                        [:img#icon-preview {:style "width:80px;height:80px" :src (str "/images/icon/" (:icon current-user))}]]]
                      (hf/hidden-field {:id "is-icon-delete?"} "is-icon-delete?" "false")
                      [:div.uk-margin
                       [:label.uk-form-label {:for "edit-password"} "PASSWORD"]
                       [:div.uk-form-controls
                        (hf/password-field
                          {:class "uk-input" :id "edit-password" :placeholder "Input Password"}
                          "password"
                          (h (:password current-user)))]]       ; 本来は別画面
                      [:div.uk-text-right
                       (hf/submit-button {:class "uk-button uk-button-default"} "UPDATE")]])]]
       (layout/common req "Edit User")))

(defn user-delete-view [req]
  (->> [:main#main
        [:div.uk-flex.uk-margin.uk-card.uk-card-default.uk-card-body.card-user-register
         (hf/form-to {:class "login-form"}
                     [:delete (get-path :user-destroy)]
                     (anti-forgery-field)
                     [:fieldset.uk-fieldset
                      [:legend.uk-legend "USER DELETE"]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "delete-login-name"} "LOGIN ID"]
                       [:div.uk-form-controls
                        (hf/text-field {:class "uk-input" :id "delete-login-name" :placeholder "Input Login Id"} "login-name")]]
                      [:div.uk-margin
                       [:label.uk-form-label {:for "delete-password"} "PASSWORD"]
                       [:div.uk-form-controls
                        (hf/password-field {:class "uk-input" :id "delete-password" :placeholder "Input Password"} "password")]]
                      [:div
                       [:p "Are you sure you want to delete this user?"]]
                      [:div.uk-text-right
                       (hf/submit-button {:class "uk-button uk-button-danger"} "DELETE")]])]]
       (layout/common req "Delete User")))


(ns earth-clj.component
  (:use [org.httpkit.server :only [run-server]])
  (:require [com.stuartsierra.component :as component]
            [taoensso.sente :as sente]
            [environ.core :refer [env]]
            [earth-clj.db :as db]
            [earth-clj.mailer :as mailer]
            [clojure.java.jdbc :as jdbc]
            [clojure.core.async :refer [go-loop <! >! chan] :as async]
            [earth-clj.socket :as socket]
            [migratus.core :as migratus]
            [earth-clj.wolfram :as wolfram]
            [earth-clj.core :as earth]))

;; 基本的には変更可能な状態をコンポーネントに押し込めるイメージ
;; 単純にrefやatomで持ってたものをrouter_などのローカル変数に押し込める

(defrecord Mailer [host user pass ssl port mailer]
  component/Lifecycle
  (start [this]
    (if mailer
      this
      (let [mailer-ch (chan)]
        (println ";; Starting Mailer Channel")
        (mailer/start-mailer {:host host :user user :pass pass :ssl ssl :port port} mailer-ch)
        (assoc this :mailer mailer-ch))))
  (stop [this]
    (if (not mailer)
      this
      (do
        (println ";; Stop Mailer Channel")
        (assoc this :mailer nil)))))

(defn create-mailer [host user pass ssl port]
  (map->Mailer {:host host :user user :pass pass :ssl ssl :port port}))

(defrecord Migrator [migrate-spec] 
  component/Lifecycle
  (start [this]
    (do (println ";; Starting Migration Database")
        (migratus/migrate migrate-spec)
        (assoc this :migrate :migrated)))
  (stop [this] 
    (assoc this :migrate nil)))

(defn create-migrator [migrate-spec]
  (map->Migrator {:migrate-spec migrate-spec}))

;; データベースコンポーネント
(defrecord Database [db-spec connection]
  component/Lifecycle
  (start [this]
    (if connection
      this
      (do (println ";; Starting Relational Database")
          (let [conn (jdbc/get-connection db-spec)]
            (assoc this :connection conn)))))
  (stop [this]
    (if (not connection)
      this
      (do (try  (.close (:connection this))
                (catch Throwable t
                  ";; Error when stopping database"))
          (println ";; Relational Database stopped")
          (assoc this :connection nil)))))

;; データベースコンポーネントの作成用関数
(defn create-database [db-spec]
  (map->Database {:db-spec db-spec}))

(defrecord Wolfram [qasystem]
  component/Lifecycle
  (start [this]
    (let [wolfram-ch (chan)]
      (println ";; Starting Wolfram Alpha")
      (wolfram/start-wolfram-service wolfram-ch)
      (assoc this :qasystem wolfram-ch)))
  (stop [this]
    (println ";; Wolfram stopped")
    (assoc this :qasystem nil)))

(defn create-wolfram []
  (map->Wolfram {}))

;; WebSocketコンポーネント
(defrecord Socket [router]
  component/Lifecycle
  (start [this]
    (if router
      this
      (do (println ";; Starting Chat Socket")
          (let [router_ (sente/start-server-chsk-router! socket/ch-chsk socket/event-msg-handler)
                read-ch (chan)]
            (socket/watch-wolfram-service read-ch)
            (assoc this :reader read-ch :router router_)))))
  (stop [this]
    (if (not router)
      this
      ;; router自身が終了用の関数 
      (do (try (router)         
               (catch Throwable t
                 ";; Error when stopping database"))
          (println ";; Socket stopped")
          (assoc this :reader nil :router nil)))))

;; Socketコンポーネントの作成用関数
(defn create-socket []
  (map->Socket {}))

;; requestにQAコンポーネントとその受信用チャネルを追加するミドルウェア
(defn wrap-app-component [f qa reader db mailer]
  (fn [req]
    (f (assoc req :reader reader :qasystem qa :db db :mailer mailer))))

;; ミドルウェアを適用したringハンドラを返す関数
(defn make-handler [qa reader db mailer]
  (wrap-app-component earth/app qa reader db mailer))

(defrecord Server [server host port join? router qasystem db mailer]
  component/Lifecycle
  (start [this]
    (if server
      this
      (do (println ";; Starting HTTP Server")
          (let [server (run-server (make-handler (:qasystem qasystem) (:reader router) db (:mailer mailer))
                                   {:host  host
                                    :port  port
                                    :join? join?})]
            (assoc this :server server)))))
  (stop [this]
    (if (not server)
      this
      (do (try (server)                                     ;; http-kitの終了
               (catch Throwable t
                 (print ";; Error when stopping HTTP server")))
          (println ";; HTTP server stopped")
          (assoc this :server nil)))))

;; HTTPサーバコンポーネント
(defn create-http-server [host port join?]
  ;; map->ReacodNameで引数に与えられたMapからレコードを生成する
  (map->Server {:host host :port port :join? join?}))

;; システム作成用関数
(defn create-system [& {:keys [host port join?]
                        :or   {host "localhost" port 4000 join? false}}]
  (component/system-map
   :qasystem (create-wolfram)
   :db (create-database db/db-spec)
   :router (create-socket) 
   :mailer (create-mailer (:mail-host env) (:mail-user env) (:mail-pass env) (= "true" (:mail-ssl env)) (Integer/parseInt (:mail-port env)))
   :migrator (component/using
              (create-migrator db/migrate-spec)
              [:db])
   :server (component/using
            (create-http-server host port join?)
            [:db :router :qasystem :mailer])))


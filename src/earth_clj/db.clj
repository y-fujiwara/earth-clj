(ns earth-clj.db
  (:require [clojure.java.jdbc :as jdbc]
            [environ.core :refer [env]]))

(def db-spec
  (if (map? (read-string (:db env)))
    ;; 冗長だが応急処置
    (read-string (:db env))
    (:db env)))

(def migrate-spec
  {:store         :database
   :migration-dir "migrations"
   :db            db-spec})
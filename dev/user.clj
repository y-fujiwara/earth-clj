(ns user
  (:require [com.stuartsierra.component :as component]
            [clojure.tools.namespace.repl :refer (refresh)]
            [migratus.core :as migratus]
            [earth-clj.db :as db]
            [earth-clj.component :as app]))

(def system nil)

(defn migration []
  (migratus/migrate db/migrate-spec))

;; システム初期化
;; alter-var-rootにしてるのはcoreのサーバー起動と同様
(defn init []
  (alter-var-root #'system
                  (constantly (app/create-system))))

;; システム起動
(defn start []
  (alter-var-root #'system component/start))

;; システム停止
(defn stop []
  (alter-var-root #'system
                  (fn [s] (when s (component/stop s)))))

;; 初期化＆起動
(defn go []
  (init)
  (start))

;; システムリロード
(defn reset []
  (stop)
  (refresh :after 'user/go))
